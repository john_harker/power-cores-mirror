# Power Cores #
3-5 people. 3min and 5max, unless a staff member wants to join.

## Gameplay: ##
Each player gets to pick 1 of 3 kits to play with. Each kit has an upgrade and an ability. (Upgrades: Starter, Normal, Pro, and Legend)

## Upgrade: ##
Every kit can be upgraded to be stronger. While it upgrades there is a time on the upgrade. You can either wait for the time to finish or you can use "credits" to speed it up.
1 credit speeds up waiting time by 10 minutes.

## How to start: ##
The game starts in a waiting lobby, a bit like the ones that Mineplex have, but is different in many ways. One of the ways it is different is how you pick a class.
After the game gets the correct amount of players (usually 5) a 30 second countdown begins - this allows any staff who want to join, time to join. Once that runs out, the players are assigned to teams and teleported to their respective bases.

## Base/HQ: ##
Each base has its own "Power Core". The members of each team respawn inside their respective bases provided the "Power Core" is still there, however, if their power core is destroyed then that team can no longer respawn.

## Respawn: ##
Respawn time is 12 seconds (If a player's core is destroyed while that player is already in respawn "mode" he can still respawn at the end of the time).

## How to destroy a power core: ##
Each power core has a shield or defence protecting it (Guardian Laser?). To disable the aforementioned defences, the team must capture the capture point (in middle of map). If a team tries to attack another team and they don't have, or lose possession of the capture point, they start to take damage from the defending teams defences.
Each power core will have 5k health. This can be displayed on a hologram.
### Suggested Holograms: ###
Line 1: &5&lPOWER CORE

Line 2: &c[&e#/5000&c]

Line 3: --DAMAGE INDICATOR--

--EXAMPLE PICTURED BELOW--
![2015-12-20_21.24.48.png](https://bitbucket.org/repo/745aer/images/1803849246-2015-12-20_21.24.48.png)

## Capture Point: ##
The capture point takes 24 seconds to capture if there is 1 person on it. If 2 then it takes 16 seconds, If 3, it takes 8 seconds If 4+ then 4 seconds.

## End: ##
The game ends once all the opposing team's power cores are destroyed and the players of opposing teams are killed. If this does not happen in 20 minutes, it is a force tie and each team member, that is alive, gets half the reward they would normally get.

## Scoreboard: ##
The scoreboard shows the amount of players alive on both teams and who is controlling the capture point. If a "Power Core" is being attacked then players of that team get a title telling them this, accompanied by an anvil breaking sound when the text comes to them. (This only happens to the team having their "Power Core" attacked.)