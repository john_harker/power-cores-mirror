package com.goldblockstudios.masterdoctor;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{


///////////////////////////////////////////////////////////////////////////////////////////
//   ____                                       ____                                     //
//  /\  _`\                                    /\  _`\                                   //
//  \ \ \L\ \___   __  __  __     __   _ __    \ \ \/\_\    ___   _ __    __    ____     //
//   \ \ ,__/ __`\/\ \/\ \/\ \  /'__`\/\`'__\   \ \ \/_/_  / __`\/\`'__\/'__`\ /',__\    //
//    \ \ \/\ \L\ \ \ \_/ \_/ \/\  __/\ \ \/     \ \ \L\ \/\ \L\ \ \ \//\  __//\__, `\   //
//     \ \_\ \____/\ \___x___/'\ \____\\ \_\      \ \____/\ \____/\ \_\\ \____\/\____/   //
//      \/_/\/___/  \/__//__/   \/____/ \/_/       \/___/  \/___/  \/_/ \/____/\/___/    //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   ____                                           __                  ____                    __                     //
//  /\  _`\               /'\_/`\                  /\ \__              /\  _`\                 /\ \__                  //
//  \ \ \L\ \  __  __    /\      \     __      ____\ \ ,_\    __   _ __\ \ \/\ \    ___     ___\ \ ,_\   ___   _ __    //
//   \ \  _ <'/\ \/\ \   \ \ \__\ \  /'__`\   /',__\\ \ \/  /'__`\/\`'__\ \ \ \ \  / __`\  /'___\ \ \/  / __`\/\`'__\  //
//    \ \ \L\ \ \ \_\ \   \ \ \_/\ \/\ \L\.\_/\__, `\\ \ \_/\  __/\ \ \/ \ \ \_\ \/\ \L\ \/\ \__/\ \ \_/\ \L\ \ \ \/   //
//     \ \____/\/`____ \   \ \_\\ \_\ \__/.\_\/\____/ \ \__\ \____\\ \_\  \ \____/\ \____/\ \____\\ \__\ \____/\ \_\   //
//      \/___/  `/___/> \   \/_/ \/_/\/__/\/_/\/___/   \/__/\/____/ \/_/   \/___/  \/___/  \/____/ \/__/\/___/  \/_/   //
//                 /\___/                                                                                              //
//                 \/__/                                                                                               //
//                     __      __  __                                                                                  //
//                    /\ \    /\ \/\ \                                                                                 //
//     __      ___    \_\ \   \ \ \/'/'  _ __   __  __    ___ ___     ____    ___     ___                              //
//   /'__`\  /' _ `\  /'_` \   \ \ , <  /\`'__\/\ \/\ \ /' __` __`\  /',__\  / __`\ /' _ `\                            //
//  /\ \L\.\_/\ \/\ \/\ \L\ \   \ \ \\`\\ \ \/ \ \ \_\ \/\ \/\ \/\ \/\__, `\/\ \L\ \/\ \/\ \                           //
//  \ \__/.\_\ \_\ \_\ \___,_\   \ \_\ \_\ \_\  \/`____ \ \_\ \_\ \_\/\____/\ \____/\ \_\ \_\                          //
//   \/__/\/_/\/_/\/_/\/__,_ /    \/_/\/_/\/_/   `/___/> \/_/\/_/\/_/\/___/  \/___/  \/_/\/_/                          //
//                                                  /\___/                                                             //
//                                                  \/__/                                                              //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//VARIABLES
	
	PluginDescriptionFile pdfFile = this.getDescription();
	String prefix = "Power Cores" //Without brackets!
	
	public void onEnable(){
		
		Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[" + prefix + "] Enabled Power Cores!");
		
	}
	
	public void onDisable(){
		
		Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + prefix + "] Disabled Power Cores!");
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		
		if(cmd.getName.equalsIgnoreCase("pcores"){
			
			if(args.length == 1){
				
				if(args[0].equalsIgnoreCase("version") || args[0].equalsIgnoreCase("info")){
					
					sender.sendMessage(ChatColor.GREEN + "Power Cores v" + pdfFile.getVersion());
					sender.sendMessage(ChatColor.GREEN + "By MasterDoctor and Krymson!");
					
				}
				
			}else{
			
				errorMsg(sender, null, 0, "Usage: /pcores <command>");
				
			}
			
		}
	
		return false;
	
	}
	
	public void errorMsg(CommandSender sender, Player player, int errorCode, String extra){
	
		String[] errorCodes;
		errorCodes = new String[1];
		
		errorCodes[0] = "Incorrect arguments!";
		
		String errorMsg = ChatColor.RED + "[" + prefix + "] An error has occured! " + errorCodes[int];
		
		if(extra == null || extra == ""){}else{
			
			errorMsg = errorMsg + " " + extra;
			
		}
		
		if(sender == null){
			//Sender is a player.
			
			player.sendMessage(errorMsg);
						
		}else{
			//Sender is console.
			
			sender.sendMessage(errorMsg);
			
		}
		
	}

}
